import React, { Component } from 'react';
import { Table, Divider, Tag } from 'antd';
import {
    Button
} from 'antd';
const { Column, ColumnGroup } = Table;
const data = [{
    key: '1',
    firstName: 'John',
    lastName: 'Brown',
    email: "john.brown@gmail.com",
}, {
    key: '2',
    firstName: 'Jim',
    lastName: 'Green',
    email: "jim.green@gmail.com",
}, {
    key: '3',
    firstName: 'Joe',
    lastName: 'Black',
    email: "joe.black@gmail.com",
}];

class Accounts extends React.Component {
    render() {
        return (
            <React.Fragment>
                <div style={{padding: "30px", float: "left", marginLeft: "300px"}}>
                    <Button type="primary" onClick={this.enterLoading}>
                        Add User
                    </Button>
                    <div style={{marginTop: "20px", padding: "30px", width: "900px"}}>
                        <Table dataSource={data}>
                            <ColumnGroup title="Name">
                            <Column
                                title="First Name"
                                dataIndex="firstName"
                                key="firstName"
                            />
                            <Column
                                title="Last Name"
                                dataIndex="lastName"
                                key="lastName"
                            />
                            </ColumnGroup>
                            <Column
                            title="Email"
                            dataIndex="email"
                            key="email"
                            />
                            <Column
                            title="Action"
                            key="action"
                            render={(text, record) => (
                                <span>
                                <a href="javascript:;">Disable {record.lastName}</a>
                                <Divider type="vertical" />
                                <a href="javascript:;">Delete</a>
                                </span>
                            )}
                            />
                        </Table>
                    </div>
                </div>
                
            </React.Fragment>
        );
    }
}

export default Accounts;