import React, { Component } from 'react';
// import Button from "antd/lib/button/";
import {
    Checkbox, Button, Card, Form, Input
} from 'antd';

const CheckboxGroup = Checkbox.Group;

const plainOptions = ['Tickets', 'Contacts', 'Companies'];
const defaultCheckedList = ['Apple', 'Orange'];

class PeriodData extends React.Component {

    state = {
        checkedList: defaultCheckedList,
        indeterminate: true,
        checkAll: false,
        loading: false,
        formLayout: 'vertical',
    }

    enterLoading = () => {
        this.setState({ loading: true });
    }

    onChange = (checkedList) => {
        this.setState({
            checkedList,
            indeterminate: !!checkedList.length && (checkedList.length < plainOptions.length),
            checkAll: checkedList.length === plainOptions.length,
        });
    }
    
    onCheckAllChange = (e) => {
        this.setState({
            checkedList: e.target.checked ? plainOptions : [],
            indeterminate: false,
            checkAll: e.target.checked,
        });
    }

    render() {
        const { loading } = this.state;
        const { formLayout } = this.state;
        const formItemLayout = formLayout === 'horizontal' ? {
            labelCol: { span: 4 },
            wrapperCol: { span: 14 },
        } : null;
        const buttonItemLayout = formLayout === 'horizontal' ? {
        wrapperCol: { span: 14, offset: 4 },
        } : null;

        return (
            <React.Fragment>
                <div style={{ borderBottom: '1px solid #E9E9E9', marginTop: "30px" }}>
                    <Card 
                        style={{ background: "#ECECEC", marginLeft: "200px"}} 
                        loading={loading}>
                        <Checkbox
                        indeterminate={this.state.indeterminate}
                        onChange={this.onCheckAllChange}
                        checked={this.state.checkAll}
                        >
                            Check all
                        </Checkbox>
                        <br />
                        <CheckboxGroup style={{marginTop: "20px"}} options={plainOptions} value={this.state.checkedList} onChange={this.onChange} />
                        <div style={{marginTop: "30px", marginLeft: "400px", width: "500px", textAlign: "center"}}>
                            <Form layout={formLayout}>
                                <Form.Item
                                    label="From"
                                    {...formItemLayout}
                                >
                                    <Input placeholder="input placeholder" />
                                </Form.Item>
                                <Form.Item {...buttonItemLayout} style={{marginLeft: "-100px"}}>
                                    <Button type="primary" loading={this.state.loading} onClick={this.enterLoading}>
                                        Set
                                    </Button>
                                </Form.Item>
                            </Form>
                        </div>
                    </Card>
                </div>
            </React.Fragment>
        );
    }
}

export default PeriodData;