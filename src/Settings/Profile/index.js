import React, { Component } from 'react';
// import Button from "antd/lib/button/";
import {
    Card, Form, Input, Button
} from 'antd';

const { Meta } = Card;

class Profile extends React.Component {

    state = {
        formLayout: 'vertical',
        loading: false
    };

    enterLoading = () => {
        this.setState({ loading: true });
    }

    render() {
        const { formLayout } = this.state;
        const buttonItemLayout = formLayout === 'horizontal' ? {
        wrapperCol: { span: 14, offset: 4 },
        } : null;
        const formItemLayout = {
            labelCol: { span: 6 },
            wrapperCol: { span: 14 },
        };
        return (
            <React.Fragment>
                <div style={{background: "#ECECEC", padding: "15px", marginLeft: "200px"}}>
                    <Card title="Settings" bordered={true} style={{ width: "700px", marginLeft: "100px"}} >
                        <Form {...formItemLayout}>
                            <Form.Item label="API Key">
                                <span className="ant-form-text" style={{display: "inline-block", textAlign: "right", width: "140px"}}>AWJalqojqKkxyfcW</span>
                            </Form.Item>
                            <Form.Item label="Domain">
                                <span className="ant-form-text">convergytics.freshdesk.com</span>
                            </Form.Item>
                            <Form.Item label="Freshdesk API Key">
                                <span className="ant-form-text">YDE0ZE3teJdGZzQ1JfsV</span>
                            </Form.Item>
                            <Form.Item>
                                <Button type="primary" onClick={this.renderPopup} style={{display: "inline-flex"}}>
                                    Edit
                                </Button>
                            </Form.Item>
                        </Form>
                    </Card>
                </div>
                <div style={{background: "#ECECEC", padding: "15px", marginLeft: "200px"}}>
                    <Card title="Change Password" bordered={true} style={{width: "700px", marginLeft: "100px"}}>
                        <Form layout={formLayout}>
                            <Form.Item
                                label="Current Password"
                                {...formItemLayout}
                            >
                                <Input type="password" placeholder="input placeholder" />
                            </Form.Item>
                            <Form.Item
                                label="New Password"
                                {...formItemLayout}
                            >
                                <Input type="password" placeholder="input placeholder" />
                            </Form.Item>
                            <Form.Item
                                label="Re-Enter New Password"
                                {...formItemLayout}
                            >
                                <Input type="password" placeholder="input placeholder" />
                            </Form.Item>
                            <Form.Item {...buttonItemLayout}>
                                <Button type="primary" loading={this.state.loading} onClick={this.enterLoading}>
                                    Change Password
                                </Button>
                            </Form.Item>
                        </Form>
                    </Card>
                </div>
            </React.Fragment>
        );
    }
}

export default Profile;
