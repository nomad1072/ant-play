import React, { Component } from 'react';
// import Button from "antd/lib/button/";
import {
    Card
} from 'antd';
import TicketProperty from "./TicketProperty";
import TicketActivity from "./TicketActivity";

const { Meta } = Card;

class DailyDump extends React.Component {

    state = {
        loading: true
    }

    onChange = (checked) => {
        this.setState({ loading: false });
    }

    componentDidMount = () => {
        this.onChange();
    }

    render() {
        const { loading } = this.state;

        return (
            
            <React.Fragment>
                <div>
                    <div style={{background: "#ECECEC", padding: "30px", marginLeft: "200px"}}>
                        <Card 
                            style={{ width: 700, marginLeft: "200px"}} 
                            loading={loading}>
                            <TicketProperty />
                        </Card>
                    </div>
                    <div style={{background: "#ECECEC", padding: "30px", marginLeft: "200px"}}>
                        <Card
                            style={{ width: 700, marginLeft: "200px" }}
                            loading={loading}
                        >
                            <TicketActivity />
                        </Card>
                    </div>
                </div>
            </React.Fragment>
        );
    }
}

export default DailyDump;