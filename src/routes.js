import React from "react";
import { BrowserRouter ,Switch, Route } from "react-router-dom";

import Custom from "./Dashboard/Custom";
import Generic from "./Dashboard/Generic";

import Dailydump from "./Features/Dailydump";
import Perioddata from "./Features/Perioddata";
import Realtime from "./Features/Realtime";

import Profile from "./Settings/Profile";
import Accounts from "./Settings/Accounts";


// import Page404 from "./views/theme/pages/404";

const routes = props => (
    <Switch>
        <Route 
            exact
            render={routeProps => <Custom {...props} {...routeProps}/>}
            path="/"
        />
        <Route
            exact
            render={routeProps => <Custom {...props} {...routeProps} />}
            path="/dashboard/custom"
        />
        <Route
            exact
            render={routeProps => <Generic {...props} {...routeProps} />}
            path="/dashboard/generic"
        />
        <Route
            render={routeProps => <Realtime {...props} {...routeProps} />}
            path="/features/real-time"
        />
        <Route
            render={routeProps => <Dailydump {...props} {...routeProps} />}
            path="/features/daily-dump"
        />
        <Route
            render={routeProps => <Perioddata {...props} {...routeProps} />}
            path="/features/period-data"
        />
        <Route
            render={routeProps => <Accounts {...props} {...routeProps} />}
            path="/settings/accounts"
        />
        <Route 
            render={routeProps => <Profile {...props} {...routeProps} />}
            path="/settings/profile"
        />
    </Switch>
);

export default routes;
