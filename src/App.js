import React, { Component } from 'react';
// import Button from "antd/lib/button/";
import {
  Layout, Menu, Breadcrumb, Icon,
} from 'antd';
import './App.css';
import Routes from './routes';
import { Link } from "react-router-dom";


const { SubMenu } = Menu;
const { Header, Content, Sider } = Layout;

// import logo from './logo.svg';

class App extends Component {
  rootSubmenuKeys = ['sub1', 'sub2', 'sub3','sub4'];

  state = {
    openKeys: ['sub1']
  };
  
  onOpenChange = (openKeys) => {
    const latestOpenKey = openKeys.find(key => this.state.openKeys.indexOf(key) === -1);
    if (this.rootSubmenuKeys.indexOf(latestOpenKey) === -1) {
      this.setState({ openKeys });
    } else {
      this.setState({
        openKeys: latestOpenKey ? [latestOpenKey] : [],
      });
    }
  }

  onMenuClick = ({item, key, keyPath}) => {
    console.log('Item: ', item);
    console.log('Key: ', key);
    console.log('Key Path: ', keyPath);
  }

  render() {
    return (
      <div className="App">
        <Layout>
          <Header className="header">
            {/* <div className="logo">
              <img className="logo" src="Blik-logo.png" />
            </div> */}
            <Menu
              theme="dark"
              mode="horizontal"
              defaultSelectedKeys={['2']}
              style={{ lineHeight: '64px' }}
            >
              <Menu.Item key="1" style={{"float": "left"}}>
                  <img className="logo" src="Blik-logo.png" />
              </Menu.Item>
              <Menu.Item key="3" style={{"float": "right"}}>
                <Icon type="logout" style={{ color: "white"}} />
              </Menu.Item>
              <Menu.Item key="2" style={{"float": "right"}}>siddharthlanka@gmail.com</Menu.Item>
            </Menu>
            <div className="logout-container">
              <Icon type="logout" style={{ color: "white"}} spin={true}/>
              <span style={{ color: "white"}}>Logout</span>
            </div>
          </Header>
          <Layout>
            <Sider width={200} style={{ background: '#fff', height: '100vh', position: 'fixed' }}>
              <Menu
                theme="dark"
                mode="inline"
                openKeys={this.state.openKeys}
                onOpenChange={this.onOpenChange}
                // onClick={this.onMenuClick}
                defaultSelectedKeys={['1']}
                defaultOpenKeys={['sub1']}
                style={{ height: '100%', borderRight: 0 }}
              >
                <SubMenu key="sub1" title={<span><Icon type="user" style={{"float": "left", "paddingTop": "12px"}}/>Dashboard</span>}>
                  <Menu.Item key="1">
                    <Link to="/dashboard/custom">
                      <Icon type="form" />
                      <span>Custom</span>
                    </Link>
                  </Menu.Item>
                  <Menu.Item key="2">
                    <Link to="/dashboard/generic">
                      <Icon type="form" />
                      <span>Generic</span>
                    </Link>
                  </Menu.Item>
                </SubMenu>
                <SubMenu key="sub2" title={<span><Icon type="laptop" style={{"float": "left", "paddingTop": "12px"}} />Features</span>}>
                  <Menu.Item key="3">
                    <Link to="/features/real-time">
                      <Icon type="form" />
                      <span>Real Time</span>
                    </Link>
                  </Menu.Item>
                  <Menu.Item key="4">
                    <Link to="/features/daily-dump">
                        <Icon type="form" />
                        <span>Daily Dump</span>
                    </Link>
                  </Menu.Item>
                  <Menu.Item key="5">
                    <Link to="/features/period-data">
                      <Icon type="form" />
                      <span>Period Data</span>
                    </Link>
                  </Menu.Item>
                </SubMenu>
                <SubMenu key="sub3" title={<span><Icon type="notification" style={{"float": "left", "paddingTop": "12px"}} />Settings</span>}>
                  <Menu.Item key="6">
                    <Link to="/settings/profile">
                      <Icon type="form" />
                      <span>Profile</span>
                    </Link>
                  </Menu.Item>
                  <Menu.Item key="7">
                    <Link to="/settings/accounts">
                      <Icon type="form" />
                      <span>Accounts</span>
                    </Link>
                  </Menu.Item>
                </SubMenu>
              </Menu>
            </Sider>
            <Layout style={{ backgroundColor: '#ffffff' }}>
              <Content style={{ margin: '0 16px' }}>
                <Routes />
              </Content>
            </Layout>
          </Layout>
        </Layout>
      </div>
    );
  }
}

export default App;
