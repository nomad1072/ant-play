import React, { Component } from 'react';
// import Button from "antd/lib/button/";
import {
    Checkbox, Button
} from 'antd';


const CheckboxGroup = Checkbox.Group;

const plainOptions = ['Ticket Create', 'Ticket Update', 'Contact Create', 'Contact Update', 'Converstion Update'];
const defaultCheckedList = ['Apple', 'Orange'];

class Realtime extends React.Component {
    state = {
        checkedList: defaultCheckedList,
        indeterminate: true,
        checkAll: false,
        loading: false
    }

    enterLoading = () => {
        this.setState({ loading: true });
    }

    onChange = (checkedList) => {
        this.setState({
            checkedList,
            indeterminate: !!checkedList.length && (checkedList.length < plainOptions.length),
            checkAll: checkedList.length === plainOptions.length,
        });
    }
    
    onCheckAllChange = (e) => {
        this.setState({
            checkedList: e.target.checked ? plainOptions : [],
            indeterminate: false,
            checkAll: e.target.checked,
        });
    }

    render() {
        return (
            <React.Fragment>
                <div style={{ borderBottom: '1px solid #E9E9E9', marginTop: "30px" }}>
                    <Checkbox
                        indeterminate={this.state.indeterminate}
                        onChange={this.onCheckAllChange}
                        checked={this.state.checkAll}
                    >
                        Check all
                    </Checkbox>
                </div>
                <br />
                <CheckboxGroup options={plainOptions} value={this.state.checkedList} onChange={this.onChange} />
                <div style={{"marginTop": "20px", marginLeft: "0px"}}>
                    <Button type="primary" loading={this.state.loading} onClick={this.enterLoading}>
                        Save
                    </Button>
                </div>
            </React.Fragment>
        );
    }
}

export default Realtime;