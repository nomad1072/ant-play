import React, { Component } from 'react';
// import Button from "antd/lib/button/";
import {
    Card, Form, Input, Button
} from 'antd';

const { Meta } = Card;

class TicketProperty extends React.Component {

    state = {
        formLayout: 'vertical',
        loading: false
    };

    enterLoading = () => {
        this.setState({ loading: true });
    }

    render() {
        const { formLayout } = this.state;
        const formItemLayout = formLayout === 'horizontal' ? {
            labelCol: { span: 4 },
            wrapperCol: { span: 14 },
        } : null;
        const buttonItemLayout = formLayout === 'horizontal' ? {
        wrapperCol: { span: 14, offset: 4 },
        } : null;

        return (
            <Form layout={formLayout}>
                <Form.Item
                    label="Ticket Property Update URL"
                    {...formItemLayout}
                >
                    <Input placeholder="input placeholder" />
                </Form.Item>
                <Form.Item
                    label="Time"
                    {...formItemLayout}
                >
                    <Input placeholder="input placeholder" />
                </Form.Item>
                <Form.Item {...buttonItemLayout}>
                    <Button type="primary" loading={this.state.loading} onClick={this.enterLoading}>
                        Set
                    </Button>
                </Form.Item>
            </Form>
        );
    }
}

export default TicketProperty;